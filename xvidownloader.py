import argparse

version = __import__("sys").version_info[0]

if version >= 3:
	import urllib.request as u
else:
	import urllib.request as u
	
parser = argparse.ArgumentParser(description="descarga fracciones de video de xvideos")
parser.add_argument('--video', help='url del video')
parser.add_argument('--rf', help="rango final")
parser.add_argument('--nombre', help="nombre del video")
parser.add_argument('-hash', help="hash (opcional)")
parser.add_argument('-e', help="e (opcional)")
parser.add_argument('-l', help="l (opcional)")

args = parser.parse_args()

prot = '' 
if args.hash:
	prot = prot + '?' + 'h=' + args.hash
if args.e:
	prot = prot + '&e=' + args.e 
if args.l:
	prot = prot + '&l=' + args.l

with open(args.nombre,"ab") as v:

	print('Intentando descarga')

	for parte in range(1,int(args.rf) + 1):
		url = str(args.video) + str(parte) + ".ts" + prot
		video = u.urlopen(url)
		v.write(video.read())

print('Proceso terminado')
